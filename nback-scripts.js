nback_member_functions = function () {

    // nback variables
    var nbackFlag = false;
    var twoBackDurationMinutes = 10;
    var my_duration = twoBackDurationMinutes * 60 * 1000;
    var shouldHandleKeyDown = true;
    var myGrid;
    var myObject;
    var shiftVarX = 0;
    var shiftVarY = 0;

    var preVarX = [];
    var preVarY = [];

    var keyPress = false;
    var nbackMatch = false;
    var beginTest = false;
    var nVal;
    var mode = 'testing';
    var timeVal = 4;
    var startTimeTest;
    var endTimeTest;
    var startTimeCirc;


    function start_nback() {
        nbackFlag = true;

        startTimeTest = new Date();
        document.getElementById("input_form").style.display = "none";
        document.getElementById("subjectiveForm").style.display = "none";
        document.getElementById("theHead").style.display = "none";

        var nback_intro = setInterval(function () {

            document.getElementById("block-intro").style.display = "block";
            timeVal = timeVal - 1;
            document.getElementById("block-intro").innerHTML = "< Two-back task begins in " + timeVal + ' >';

            if (timeVal == 0) {

                document.getElementById("block-intro").style.display = "none";
                // console.log(timeVal);
                timeVal = 4
                clearInterval(nback_intro);
                // return;
                myGrid = new drawGrid();
                myObject = new drawCircle();


                nVal = 2; // two-back task
                myGameArea.start();

            }
        }, 1000);



    }

    var myGameArea = {

        canvas: document.createElement("canvas"),
        start: function () {
            this.canvas.id = 'mycanvas';
            this.canvas.width = 400;
            this.canvas.height = 400;
            this.canvas.style.position = "relative";
            this.canvas.style.top = "20%";
            this.context = this.canvas.getContext("2d");
            this.context.fillStyle = 'black';
            this.context.fillRect(0, 0, this.canvas.height + 10, this.canvas.width + 10);
            document.body.insertBefore(this.canvas, document.body.childNodes[0]);
            this.frameNo = 0;
            nback_interval = setInterval(twoback_block, 2000); // game are updated every 2s
            // document.addEventListener("keypress", keyEvent, true);
        },

        clear: function () {
            this.context.clearRect(0, 0, this.canvas.width, this.canvas.height);
        }
    }

    function drawGrid() {
        this.update = function () {
            ctx = myGameArea.context;
            gridLineWidth = 5;
            ctx.fillStyle = "black";
            ctx.fillRect(0, 0, ctx.canvas.height, ctx.canvas.width);
            ctx.fillStyle = "gray";
            ctx.fillRect(0, ctx.canvas.height / 3 - gridLineWidth / 2, ctx.canvas.width, gridLineWidth);
            ctx.fillRect(0, 2 * (ctx.canvas.height / 3) - gridLineWidth / 2, ctx.canvas.width, gridLineWidth);
            ctx.fillRect(ctx.canvas.width / 3 - gridLineWidth / 2, 0, gridLineWidth, ctx.canvas.height);
            ctx.fillRect(2 * (ctx.canvas.width / 3) - gridLineWidth / 2, 0, gridLineWidth, ctx.canvas.height);
        }
    }

    function drawCircle() {

        this.update = function () {
            // 1 back method 
            if (nVal === 1) {
                preVarX = shiftVarX;
                preVarY = shiftVarY;
                shiftVarX = randomWithProbability(preVarX)
                shiftVarY = randomWithProbability(preVarY)
                if (shiftVarX != preVarX || shiftVarY != preVarY) { nbackMatch = false; } else { nbackMatch = true; }
            } else if (nVal === 2) {
                if (preVarX === undefined || preVarX.length < 2) {

                    preVarX.push(shiftVarX);
                    preVarY.push(shiftVarY);


                } else {

                    // begin matching with two values in arr  
                    beginTest = true;
                    // remove first element in array
                    preVarX.shift();
                    preVarY.shift();
                    // append new element into array
                    preVarX.push(shiftVarX);
                    preVarY.push(shiftVarY);
                }

                // console.log(preVarX)
                shiftVarX = randomWithProbability(preVarX[0])
                shiftVarY = randomWithProbability(preVarY[0])

                if (shiftVarX != preVarX[0] || shiftVarY != preVarY[0] && ~beginTest) { nbackMatch = false; } else { nbackMatch = true; }
            } else {
                if (preVarX === undefined || preVarX.length < 3) {

                    preVarX.push(shiftVarX);
                    preVarY.push(shiftVarY);


                } else {

                    // begin matching with two values in arr  
                    beginTest = true;
                    // remove first element in array
                    preVarX.shift();
                    preVarY.shift();
                    // append new element into array
                    preVarX.push(shiftVarX);
                    preVarY.push(shiftVarY);
                }

                // console.log(preVarX)
                shiftVarX = randomWithProbability(preVarX[0])
                shiftVarY = randomWithProbability(preVarY[0])

                if (shiftVarX != preVarX[0] || shiftVarY != preVarY[0] && ~beginTest) { nbackMatch = false; } else { nbackMatch = true; }
            }


            ctx = myGameArea.context;
            ctx.beginPath();
            ctx.arc((0.5 + shiftVarX) * (ctx.canvas.width / 3), (0.5 + shiftVarY) * (ctx.canvas.height / 3), 25, 0, 2 * Math.PI);
            ctx.lineWidth = 5;
            ctx.strokeStyle = '#003300';
            ctx.fillStyle = '#6ab04c';
            ctx.fill();
            ctx.stroke();

            startTimeCirc = new Date();
            this.timeout = setTimeout(clearArc, 900)


        }
    }

    // function for random selection
    function randomWithProbability(preVar) {
        var notRandomNumbers = [0, 1, 2, preVar];
        var idx = Math.floor(Math.random() * notRandomNumbers.length);
        return notRandomNumbers[idx];
    }

    function twoback_block() {

        endTimeTest = new Date();
        var elapsedTimeTest = endTimeTest - startTimeTest;
        // console.log(endTimeTest);
        // console.log(startTimeTest);

        if (elapsedTimeTest <= my_duration) {
            // rule for missed responses
            if (!keyPress && nbackMatch) {
                console.log('missed');
                send_data_to_server(Date().toLocaleString(), '0', ' ');

                // update error display with clear timeout
                // if (mode == 'training') {
                //     document.getElementById("nback-feedback").style.display = "block";
                //     document.getElementById("nback-feedback").innerHTML = "<span style='color: Orange;'>Missed</span>";

                //     setTimeout(function () {
                //         document.getElementById("nback-feedback").style.display = "none";
                //     }, 1000);



                // }
            }

            // clearing the interface
            myGameArea.clear();
            keyPress = false;
            myGrid.update();
            myObject.update();
        }

        else {
            // terminal conditions
            nbackFlag = false;

            clearInterval(nback_interval);
            myGameArea.clear();
            // document.getElementById("mycanvas").style.display = "none";
            document.getElementById("mycanvas").remove();
            // continueInterface();

        }
    }

    function nback_terminate_condition_check() {
        var d = $.Deferred();
        start_nback();
        var checker = setInterval(function () {
            if (nbackFlag == false) {
                console.log('nback-to-terminate');
                clearInterval(checker);
                d.resolve();
                return d.promise();
            }
        }, 10);

        return d.promise();
    }

    function clearArc() {
        myGameArea.clear();
        myGrid.update();
    }

    function drawText(x, y) {
        this.update = function (text, color) {
            ctx = myGameArea.context;
            ctx.font = "30px Comic Sans MS";
            ctx.fillStyle = color;
            ctx.textAlign = "center";
            ctx.fillText(text, ctx.canvas.width - x, y);
        }
    }

    document.onkeyup = function () {
        shouldHandleKeyDown = true;
    }

    document.body.onkeydown = function (e) {

        // handles keydown being registered as multiple events
        if (shouldHandleKeyDown) {
            shouldHandleKeyDown = false;

            keyPress = true;

            if (e.key === " " && nbackFlag === true && typeof nbackFlag !== 'undefined') {
                console.log('keypress-nback');
                endTimeCirc = new Date();
                var elapsedTimeCirc = endTimeCirc - startTimeCirc;
                if (!nbackMatch) {

                    if (mode == 'training') {

                        // update error display with clear timeout
                        document.getElementById("nback-feedback").style.display = "block";
                        document.getElementById("nback-feedback").innerHTML = "<span style='color: LightCoral;'>Error</span>";

                        setTimeout(function () {
                            document.getElementById("nback-feedback").style.display = "none";
                        }, 1000);
                    }


                    // log your data at this point
                    send_data_to_server(Date().toLocaleString(), '0', elapsedTimeCirc)


                } else {

                    // update error display with clear timeout
                    if (mode == 'training') {
                        document.getElementById("nback-feedback").style.display = "block";
                        document.getElementById("nback-feedback").innerHTML = "<span style='color: YellowGreen;'>Good</span>";

                        setTimeout(function () {
                            document.getElementById("nback-feedback").style.display = "none";
                        }, 1000);

                    }

                    send_data_to_server(Date().toLocaleString(), '1', elapsedTimeCirc)

                }
            }
        }
    }

    function send_data_to_server(rtime, rstate, rdelay) {


        // Construct form elements into Application/JSON template
        const data = {
            participantId: document.getElementById('inp').value,
            activity: "two-back-testing",
            responseTimeStamp: rtime,
            responseState: rstate,
            responseDelay: rdelay
        };

        // Send form response (sheet monkey)
        fetch('https://api.sheetmonkey.io/form/qTJhE2n844QzhYbnWHZs1i', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((result) => {
            // Handle the result
            console.log('nback-response-submitted')
        });

        return;

    }
    return {
        init: start_nback,
        setup: nback_terminate_condition_check
    }
}();
