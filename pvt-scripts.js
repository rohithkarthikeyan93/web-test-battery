pvt_member_functions = function () {

    var PVTFlag = false;
    var PVT_active = false;
    var terminatePVTFlag = false;
    var shouldHandleKeyDown = true;
    var resolvePVT = false;
    var elapsedTime;
    var timerUpdater;
    var pvt_start_time;
    var pvt_elapsed_time;
    var pvtDurationMinutes = 10;
    var pvtBlockDuration = pvtDurationMinutes * 60 * 1000;

    function PVT_instance() {
        PVT_active = true;

        var instanceStartTime = Date.now();
        pvt_elapsed_time = instanceStartTime - pvt_start_time;

        if (pvt_elapsed_time > pvtBlockDuration) {
            terminatePVTFlag = true;
            // console.log('terminate-PVT');
        }

        document.getElementById("timer").style.display = "block";
        // console.log('pvt-instance');

        // alert pvt start w/countdown
        timerUpdater = setInterval(function () {
            elapsedTime = Date.now() - instanceStartTime;
            document.getElementById("timer").innerHTML = elapsedTime;
        }, 1);
    }


    document.body.onkeyup = function () {
        shouldHandleKeyDown = true;
    }

    document.body.onkeypress = function (e) {

        if (shouldHandleKeyDown) {
            shouldHandleKeyDown = false;

            if (e.key === ' ' && PVT_active === true) {
                if (terminatePVTFlag) {
                    console.log('terminating-PVT');
                    clearInterval(timerUpdater);
                    setTimeout(function () {
                        document.getElementById("timer").style.display = "none";
                        send_data_to_server(Date().toLocaleString(), '1', elapsedTime);
                        elapsedTime = 0;
                        PVTFlag = false;
                        PVT_active = false;
                        timeVal = 4;
                        terminatePVTFlag = false;
                        resolvePVT = true;
                    }, 1000);

                    // reset your flags and variables

                } else if (PVT_active === true && !terminatePVTFlag) {
                    PVT_active = false;
                    console.log('keypress-pvt');
                    clearInterval(timerUpdater);
                    setTimeout(function () {
                        document.getElementById("timer").style.display = "none";
                        send_data_to_server(Date().toLocaleString(), '1', elapsedTime);
                        elapsedTime = 0;
                        PVTRandomizer();
                    }, 1000);

                } else {
                    console.log('pvt-patience!');
                    send_data_to_server(Date().toLocaleString(), '0', 'NA');
                }


            };
        }
    }

    function PVT_terminate_condition_check() {
        var d2 = $.Deferred();
        PVTBlock();
        pvtEndTracker = setInterval(function () {
            if (resolvePVT) {
                d2.resolve();
                resolvePVT = false;
                clearInterval(pvtEndTracker);
                return d2.promise();
            }
        }, 1);

        // $(document).keyup(function (event) {
        //     if (event.which === 32 && PVT_active === true) {
        //         if (terminatePVTFlag) {
        //             console.log('terminating-PVT');
        //             clearInterval(timerUpdater);
        //             setTimeout(function () {
        //                 document.getElementById("timer").style.display = "none";
        //                 send_data_to_server(Date().toLocaleString(), '1', elapsedTime);
        //                 elapsedTime = 0;
        //                 PVTFlag = false;
        //                 PVT_active = false;
        //                 timeVal = 4;
        //                 terminatePVTFlag = false;
        //                 d2.resolve();
        //                 return d2.promise();
        //             }, 1000);

        //             // reset your flags and variables


        //         } else if (PVT_active === true && !terminatePVTFlag) {
        //             PVT_active = false;
        //             console.log('keypress-pvt');
        //             clearInterval(timerUpdater);
        //             setTimeout(function () {
        //                 document.getElementById("timer").style.display = "none";
        //                 send_data_to_server(Date().toLocaleString(), '1', elapsedTime);
        //                 elapsedTime = 0;
        //                 PVTRandomizer();
        //             }, 1000);

        //         } else {
        //             console.log('pvt-patience!');
        //             send_data_to_server(Date().toLocaleString(), '0', 'NA');
        //         }
        //     }

        // });
        return d2.promise();

    }

    function PVTRandomizer() {
        if (!PVT_active) {
            // console.log('pvt-randomizer')
            var max = 5;
            var min = 2;
            var rand = Math.floor(Math.random() * (max - min + 1) + min); //Generate Random number between 5 - 10
            // console.log('Wait for ' + rand + ' seconds');
            setTimeout(PVT_instance, rand * 1000);
        }
    }

    function PVTBlock() {

        var timeVal = 4;
        document.getElementById("input_form").style.display = "none";
        document.getElementById("subjectiveForm").style.display = "none";
        document.getElementById("theHead").style.display = "none";
        // console.log('pvt-block')
        PVTFlag = true;

        // PVT intro blurb goes here
        var pvt_intro = setInterval(function () {

            document.getElementById("block-intro").style.display = "block";
            timeVal = timeVal - 1;
            document.getElementById("block-intro").innerHTML = "< PVT begins in " + timeVal + ' >';
            if (timeVal == 0) {

                pvt_start_time = Date.now();
                document.getElementById("block-intro").style.display = "none";
                // console.log('clear-PVT-intro')
                PVTRandomizer();
                clearInterval(pvt_intro);
                // return;

            }
        }, 1000);

    }

    function send_data_to_server(rtime, rstate, rdelay) {

        try {
            var act = document.getElementById('standard-select').value;
        } catch {
            act = "demo";
        }
        // Construct form elements into Application/JSON template
        const data = {
            participantId: document.getElementById('inp').value,
            activity: act,
            responseTimeStamp: rtime,
            responseState: rstate,
            responseDelay: rdelay
        };

        // Send form response (sheet monkey)
        fetch('https://api.sheetmonkey.io/form/c8J6fou9ezoUjZ1ALkFy6u', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify(data),
        }).then((result) => {
            // Handle the result
            // console.log('pvt-response-submitted')
        });

        return;

    }
    return {
        init: PVTBlock,
        setup: PVT_terminate_condition_check
    }
}();